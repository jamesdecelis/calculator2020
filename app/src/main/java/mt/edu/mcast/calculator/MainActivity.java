package mt.edu.mcast.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView txtvDisplay, txtvOperation;
    double num1 = 0;
    char oper = 'n'; //n considered as no operation submitted

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialization of both textViews - since these will be used through out the whole life cycle of the application
        txtvDisplay = findViewById(R.id.txtvDisplay);
        txtvOperation = findViewById(R.id.txtvOperation);
    }

    public void numberButtons(View v){

        Button btn = (Button)v;

        //check that dot is appended only once
        if(v.getId() == R.id.btnDot) {

            if (txtvDisplay.getText().toString().indexOf(".") == -1) { // -1 if . is not found in the textView

                txtvDisplay.append(btn.getText());
            }
        }else{ //other buttons
            txtvDisplay.append(btn.getText());
        }


    }

    public void delClearButtons(View v){

        if(v.getId() == R.id.btnDel){
            String content = txtvDisplay.getText().toString();
            txtvDisplay.setText(content.substring(0, content.length() -1));
        }else if(v.getId() == R.id.btnClear){
            txtvDisplay.setText("");
        }

    }

    public void operationbuttons(View v){

        Button btn = (Button)v;
        if(oper == 'n') { // there are no operations set
            num1 = Double.parseDouble(txtvDisplay.getText().toString()); //taking the first number from the textView
            oper = btn.getText().charAt(0); // setting the operation by getting the char from the button

            txtvDisplay.setText(""); //resetting the display

            txtvOperation.setText(num1 + " " + oper + " ");// showing num1 and the operation on the smaller textview

        }else {

            if(txtvDisplay.getText().length() == 0) { // change of operation if display is empty
                oper = btn.getText().charAt(0);
                txtvOperation.setText(num1 + " " + oper + " ");
            }else{//there is the second number in the textView
                double num2 = Double.parseDouble(txtvDisplay.getText().toString());
                num1 = calculate(num2); //calculating the sum

                oper = btn.getText().charAt(0);

                txtvDisplay.setText("");

                txtvOperation.setText(num1 + " " + oper + " ");
            }
        }

    }

    public void equalsButton(View v){

        double num2 = Double.parseDouble(txtvDisplay.getText().toString());

        txtvOperation.append(String.valueOf(num2));

        double ans = calculate(num2);

        oper = 'n';

        txtvDisplay.setText(String.valueOf(ans));
    }

    private double calculate(double num2){
        switch(oper){

            case '+' : return (num1 + num2);

            case '-' : return (num1 - num2);

            case '*' : return (num1 * num2);

            case '/' : return (num1 / num2);
            default : return 0;

        }
    }
}
